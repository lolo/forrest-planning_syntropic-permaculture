# Forrest planning

-research of native plant species, matching up with each other <br>
[Forest plant documentation](https://git.fairkom.net/lolo/forrest-planning_syntropic-permaculture/blob/master/Forest%20plant%20documentation)<br>

*  local pioneer trees
*  shrubs
*  edible plants
*  small size fruit trees
  
- construction of forests on theoretical basis <br>
- layer by layer (forest floor layer, shrub (or understory) layer, sub-tree layer, tree layer and canopy layer <br><br>
For the benefits of proper forest planning, it's always useful to devide the entire structure into the functional levels <br>
(canopy layer, tree layer, sub-tree layer, shrub/understory layer and forest floor layer) and thus begin with the proper initial <br>
cultivation of the most dominant native species within the highest layer. Which means that only the healthiest seedlings <br>
of such species may be used for the initial stage of agroforest development. After the first decade of constant growth, <br>
you begin with the proper cultivation of second and third layer, and so on. <br>
On the other hand, some planners begin with the intensive cultivation of all forest layers simultaneously, <br>
though that method may be a double-edged sword, due to many reasons. We have to remember that the structures of shrub <br>
and forest floor layer may actually be the toughest of all challanges, simply because they develop and advance quite rapidly, <br>
even without the proper human intervention. Sometimes we may even identify these lower layers as the bird species - <br>
they may be everywhere around, so they move quite unexpectedly, depending on the current situation within the entire forest ecosystem.

plan of a reforestation at an example after following points: 
- pick a region and then get info about the agroforest site, like altitude, common plants seen in the area, topografy info and area in square meters.  
- Doing a description of the local vegetation and picture of the area
- This info goes together with local seedlings and seeds available for free or for purchase. 
- info about the "owners" of the land, fruits and plants they like, available machinery in the land and local distributors of compost, seedlings and seeds. 
- With this info we can fill that table of levelsXtime 
- even calculate the costs of implementation.